<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Service\Payment\PaymentService;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\TwigBundle\Tests\TestCase;

class PaymentServiceTest extends TestCase
{
    private const TEST_PAYMENT_DATA_ID = 'TEST-PAYMENT-DATA-ID';

    /**
     * @var PaymentService
     */
    private $testService;

    /**
     * @var ResponseInterface
     */
    private $clientResponse;

    protected function setUp()
    {
        $this->clientResponse = new Response(
            200,
            [],
            \json_encode(['paymentDataId' => self::TEST_PAYMENT_DATA_ID])
        );

        $client = $this->createMock(ClientInterface::class);
        $client->expects($this->once())
            ->method('request')
            ->willReturnCallback(function () {
                return $this->clientResponse;
            });

        $this->testService = new PaymentService($client, '//localhost');
    }

    public function testSuccessPayment()
    {
        $paymentId = $this->testService->makePayment(123, 'DE23235', 'Doctor');

        $this->assertSame(self::TEST_PAYMENT_DATA_ID, $paymentId);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function testHttpException()
    {
        $this->clientResponse = new Response(404, [], 'Not Found!');

        $this->testService->makePayment(123, 'DE23235', 'Doctor');
    }

    /**
     * @expectedException \App\Service\Payment\InvalidServerResponse
     */
    public function testBadResponseException()
    {
        $this->clientResponse = new Response(200, [], '{error: "Unexpected Error"}');

        $this->testService->makePayment(123, 'DE23235', 'Doctor');
    }
}
