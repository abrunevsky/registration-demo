<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use Symfony\Component\HttpFoundation\Request;

interface RegistrationFormProcessorInterface extends RegistrationProcessorInterface
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function handleRequest(Request $request): void;

    /**
     * @return bool
     */
    public function isSubmittedAndValid(): bool;

    public function updateRegistrationFromRequest(): void;
}
