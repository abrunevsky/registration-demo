<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Entity\Registration;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment as TwigEnvironment;

abstract class AbstractRegistrationFormProcessor extends AbstractRegistrationProcessor implements RegistrationFormProcessorInterface
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param TwigEnvironment      $templatingEnv
     * @param FormFactoryInterface $formFactory
     * @param ObjectManager        $objectManager
     */
    public function __construct(
        TwigEnvironment $templatingEnv,
        FormFactoryInterface $formFactory,
        ObjectManager $objectManager
    ) {
        parent::__construct($templatingEnv);

        $this->form = $formFactory->create($this->getFormType());
        $this->objectManager = $objectManager;
    }

    /**
     * @param Registration|null $registration
     * @param int               $currentStep
     */
    public function setFlow(?Registration $registration, int $currentStep = 0): void
    {
        parent::setFlow($registration, $currentStep);

        if ($registration) {
            $this->form->setData($registration);
        }
    }

    /**
     * @return array
     */
    protected function getViewParameters(): array
    {
        return \array_merge(
            parent::getViewParameters(),
            ['form' => $this->form->createView()]
        );
    }

    /**
     * @param Request $request
     *
     * @return mixed|void
     */
    public function handleRequest(Request $request): void
    {
        $this->form->handleRequest($request);
    }

    /**
     * @return bool
     */
    public function isSubmittedAndValid(): bool
    {
        return $this->form->isSubmitted() && $this->form->isValid();
    }

    public function updateRegistrationFromRequest(): void
    {
        if (!$this->registration->getId()) {
            $this->objectManager->persist($this->registration);
        }

        if ($this->currentStep === $this->registration->countStatusStep()) {
            $this->registration->setNextStatus();
        }

        $this->objectManager->flush();
    }

    /**
     * @return string
     */
    abstract protected function getFormType(): string;
}
