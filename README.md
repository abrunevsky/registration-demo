Registration Demo application.
=============================
## Installation

1. Download a codebase using from the [bitbucket repocitory][bitbucket].
2. Copy `.env.dist` file into `.env`.
3. Run docker composer to build and start all the related containers. (See `make docker-up`)
4. Run composer install. (See `make composer-install`)
5. Migrate all prepared database migrations. (See `make app-migrate`)
6. Load fixtures for tables `category` and `user`. (See `make db-fixtures-load`)
7. Now you can open page [http://localhost:8080][local]

[bitbucket]:https://abrunevsky@bitbucket.org/abrunevsky/registration-demo.git
[local]:http://localhost:8080 

You can use command `make app-build` that should automatically go throw all the steps listed above.

_See file_ `Makefile` _for other helping commands_

---

### Performance optimization options to implement

1. If we do not need any server statistic about not payed users, we can save only successfully payed ones.
2. In addition (to 1), it's possible to use RESTFull approach. Frontend can realize all the steps before payment on client side only. 
   It can minimize count of requests to the server,

### What things could be done better

* The solution could contain more test cases.
* It makes sense to move docker files in a separate repository. It will allow to keep codebase clean in production.  