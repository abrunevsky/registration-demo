<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Form\Registration\AddressType;

class AddressInfoProcessor extends AbstractRegistrationFormProcessor
{
    /**
     * @return string
     */
    protected function getFormType(): string
    {
        return AddressType::class;
    }

    /**
     * @return string
     */
    protected function getView(): string
    {
        return 'registration/form/address.html.twig';
    }
}
