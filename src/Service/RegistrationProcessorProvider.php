<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Registration;
use App\Exception\InvalidStepException;
use App\Repository\RegistrationRepository;
use App\Service\RegistrationProcessor\RegistrationProcessorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationProcessorProvider
{
    /**
     * @var RegistrationRepository
     */
    private $repository;

    /**
     * @var RegistrationProcessorInterface[]
     */
    private $processorsMap;

    /**
     * @param ObjectManager $manager
     * @param array         $processorsMap
     */
    public function __construct(ObjectManager $manager, array $processorsMap)
    {
        $this->repository = $manager->getRepository(Registration::class);
        $this->processorsMap = $processorsMap;
    }

    /**
     * @param int $id
     * @param int $step
     *
     * @return RegistrationProcessorInterface
     *
     * @throws \Exception
     */
    public function getProcessor(int $id, int $step): RegistrationProcessorInterface
    {
        $registration = $this->recognizeRegistrationById($id);

        if ($step > \count(Registration::STATUSES_FLOW)) {
            throw new NotFoundHttpException();
        }

        if ($step > $registration->countStatusStep()) {
            throw new InvalidStepException('Please, be patient! Complete the previous step before.');
        }

        if (!\array_key_exists($registration->getStatus(), $this->processorsMap)) {
            throw new \Exception("Unexpected registration flow status '{$registration->getStatus()}' has been detected.");
        }

        if (Registration::STATUS_COMPLETED === $registration->getStatus()
            && $registration->countStatusStep() !== $step
        ) {
            throw new InvalidStepException('Registration is completed and cannot be changed anymore.');
        }

        $processor = $this->processorsMap[Registration::STATUSES_FLOW[$step]];
        $processor->setFlow($registration, $step);

        return $processor;
    }

    /**
     * @param int $id
     *
     * @return Registration
     *
     * @throws EntityNotFoundException
     */
    private function recognizeRegistrationById(int $id): Registration
    {
        if ($id) {
            $registration = $this->repository->find($id);

            if (!$registration) {
                throw new EntityNotFoundException("Can't find registration flow by #{$id}");
            }
        } else {
            $registration = new Registration();
        }

        return $registration;
    }
}
