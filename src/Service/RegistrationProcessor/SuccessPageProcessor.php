<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

class SuccessPageProcessor extends AbstractRegistrationProcessor
{
    /**
     * @return string
     */
    protected function getView(): string
    {
        return 'registration/success.html.twig';
    }
}
