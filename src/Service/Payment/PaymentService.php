<?php

declare(strict_types=1);

namespace App\Service\Payment;

use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PaymentService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var
     */
    private $endpointUri;

    /**
     * @param ClientInterface $client
     * @param string          $endpointUri
     */
    public function __construct(ClientInterface $client, string $endpointUri)
    {
        $this->client = $client;
        $this->endpointUri = $endpointUri;
    }

    /**
     * @param int    $customerId
     * @param string $iban
     * @param string $owner
     *
     * @return string
     *
     * @throws InvalidServerResponse
     */
    public function makePayment(int $customerId, string $iban, string $owner): string
    {
        $post = [
            'customerId' => $customerId,
            'iban' => $iban,
            'owner' => $owner,
        ];

        $response = $this->client->request('POST', $this->endpointUri, ['json' => $post]);

        $responseCode = $response->getStatusCode();
        $responseBody = $response->getBody()->getContents();

        if (Response::HTTP_OK !== $responseCode) {
            throw new HttpException($responseCode, $responseBody);
        }

        $responseData = \json_decode($responseBody, true);

        if (!isset($responseData['paymentDataId'])) {
            if ($this->logger) {
                $this->logger->error(
                    'Invalid Payment Response',
                    [
                        'endpointUri' => $this->endpointUri,
                        'post' => $post,
                        'response' => $response,
                    ]
                );
            }

            throw new InvalidServerResponse('Payment service has been returned unexpected payment response.');
        }

        return $responseData['paymentDataId'];
    }
}
