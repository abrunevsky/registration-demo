<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\RegistrationProcessorProvider;
use App\Service\RegistrationProcessor\RegistrationFormProcessorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    private const COOKIE_ID_KEY = 'regId';
    private const COOKIE_STEP_KEY = 'regStep';
    private const COOKIE_EXPIRE = '+7 days';

    /**
     * @var RegistrationProcessorProvider
     */
    private $processorProvider;

    /**
     * @param RegistrationProcessorProvider $processorProvider
     */
    public function __construct(RegistrationProcessorProvider $processorProvider)
    {
        $this->processorProvider = $processorProvider;
    }

    /**
     * @Route("/registration/new", name="registration_new")
     *
     * @return Response
     */
    public function startNew(): Response
    {
        $response = $this->redirectToRoute('registration', ['stepPlus' => 1]);

        $dateExpired = new \DateTime('-1 day');
        $response->headers->setCookie(new Cookie(self::COOKIE_ID_KEY, '', $dateExpired));
        $response->headers->setCookie(new Cookie(self::COOKIE_STEP_KEY, '', $dateExpired));

        return $response;
    }

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $step = (int) $request->cookies->get(self::COOKIE_STEP_KEY, 0);

        return $this->redirectToRoute('registration', ['stepPlus' => $step + 1]);
    }

    /**
     * @Route("/registration/{stepPlus<[1-4]>}", name="registration")
     *
     * @param int     $stepPlus
     * @param Request $request
     *
     * @return Response
     */
    public function registration(int $stepPlus, Request $request)
    {
        $id = (int) $request->cookies->get(self::COOKIE_ID_KEY, 0);

        $processor = $this->processorProvider->getProcessor($id, \max($stepPlus - 1, 0));

        if ($processor instanceof RegistrationFormProcessorInterface) {
            $processor->handleRequest($request);

            if ($processor->isSubmittedAndValid()) {
                $processor->updateRegistrationFromRequest();

                $nextStep = $processor->getCurrentStep() + 1;
                $response = $this->redirectToRoute('registration', ['stepPlus' => $nextStep + 1]);

                $cookieDateExpire = new \DateTime(self::COOKIE_EXPIRE);
                $response->headers->setCookie(
                    new Cookie(self::COOKIE_ID_KEY, (string) $processor->getFlowId(), $cookieDateExpire)
                );
                $response->headers->setCookie(
                    new Cookie(self::COOKIE_STEP_KEY, (string) $processor->getFlowStep(), $cookieDateExpire)
                );

                return $response;
            }
        }

        return new Response($processor->render());
    }
}
