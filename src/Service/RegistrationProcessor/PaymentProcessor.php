<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Form\Registration\PaymentType;
use App\Service\Payment\PaymentService;
use Doctrine\Common\Persistence\ObjectManager;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Twig\Environment as TwigEnvironment;

class PaymentProcessor extends AbstractRegistrationFormProcessor implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @param TwigEnvironment      $templatingEnv
     * @param FormFactoryInterface $formFactory
     * @param ObjectManager        $objectManager
     * @param PaymentService       $paymentService
     */
    public function __construct(
        TwigEnvironment $templatingEnv,
        FormFactoryInterface $formFactory,
        ObjectManager $objectManager,
        PaymentService $paymentService
    ) {
        parent::__construct($templatingEnv, $formFactory, $objectManager);
        $this->paymentService = $paymentService;
    }

    /**
     * @return string
     */
    protected function getFormType(): string
    {
        return PaymentType::class;
    }

    /**
     * @return string
     */
    protected function getView(): string
    {
        return 'registration/form/payment.html.twig';
    }

    /**
     * @throws \Exception
     */
    public function updateRegistrationFromRequest(): void
    {
        try {
            $paymentId = $this->paymentService->makePayment(
                $this->registration->getId(),
                $this->registration->getIban(),
                $this->registration->getAccountOwner()
            );

            $this->registration->setPaymentDataId($paymentId);
            $this->registration->setNextStatus();

            $this->objectManager->flush();
        } catch (HttpException $exception) {
            $this->objectManager->flush();

            $this->exceptionReport($exception);
        } catch (BadResponseException $exception) {
            $this->objectManager->flush();

            $this->exceptionReport($exception);
        }
    }

    /**
     * @param \Exception $exception
     */
    private function exceptionReport(\Exception $exception): void
    {
        $debugErrorMessage = \sprintf(
            '%s [%d]: %s',
            \get_class($exception),
            $exception->getCode(),
            $exception->getMessage()
        );

        if ($this->logger) {
            $this->logger->error(
                $debugErrorMessage,
                [
                    'Registration' => $this->registration,
                    'Exception' => $exception,
                ]
            );
        }

        $errorMessage = $this->templatingEnv->isDebug()
            ? $debugErrorMessage
            : 'Payment service is unavailable now. Please, try to perform this action later.';

        $this->form->addError(new FormError($errorMessage));
    }
}
