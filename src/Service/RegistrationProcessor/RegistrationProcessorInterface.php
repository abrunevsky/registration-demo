<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Entity\Registration;

interface RegistrationProcessorInterface
{
    /**
     * @return string
     */
    public function render(): string;

    /**
     * @param Registration|null $registration
     * @param int               $currentStep
     */
    public function setFlow(?Registration $registration, int $currentStep = 0): void;

    /**
     * @return int|null
     */
    public function getFlowId(): ?int;

    /**
     * @return int
     */
    public function getFlowStep(): int;

    /**
     * @return int
     */
    public function getCurrentStep(): int;
}
