<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Form\Registration\PersonalInfoType;

class PersonalInfoProcessor extends AbstractRegistrationFormProcessor
{
    /**
     * @return string
     */
    protected function getFormType(): string
    {
        return PersonalInfoType::class;
    }

    /**
     * @return string
     */
    protected function getView(): string
    {
        return 'registration/form/personal_info.html.twig';
    }
}
