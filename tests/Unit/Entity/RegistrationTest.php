<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\Registration;
use Symfony\Bundle\TwigBundle\Tests\TestCase;

class RegistrationTest extends TestCase
{
    const STATUS_ADDRESS = 'ADDRESS';
    const STATUS_PROFILE = 'PROFILE';
    const STATUS_PAYMENT = 'PAYMENT';
    const STATUS_COMPLETED = 'COMPLETED';

    /**
     * @return Registration
     */
    public function testInitialProfileStatus(): Registration
    {
        $entity = new Registration();

        $this->assertSame(self::STATUS_PROFILE, $entity->getStatus());
        $this->assertSame(0, $entity->countStatusStep());

        return $entity;
    }

    /**
     * @depends testInitialProfileStatus
     *
     * @param Registration $entity
     *
     * @return Registration
     */
    public function testAddressStatus(Registration $entity): Registration
    {
        $entity->setNextStatus();

        $this->assertSame(self::STATUS_ADDRESS, $entity->getStatus());
        $this->assertSame(1, $entity->countStatusStep());

        return $entity;
    }

    /**
     * @depends testAddressStatus
     *
     * @param Registration $entity
     *
     * @return Registration
     */
    public function testPaymentStatus(Registration $entity): Registration
    {
        $entity->setNextStatus();

        $this->assertSame(self::STATUS_PAYMENT, $entity->getStatus());
        $this->assertSame(2, $entity->countStatusStep());

        return $entity;
    }

    /**
     * @depends testPaymentStatus
     *
     * @param Registration $entity
     *
     * @return Registration
     */
    public function testCompletedStatus(Registration $entity): Registration
    {
        $entity->setNextStatus();

        $this->assertSame(self::STATUS_COMPLETED, $entity->getStatus());
        $this->assertSame(3, $entity->countStatusStep());

        return $entity;
    }

    /**
     * @depends testCompletedStatus
     *
     * @param Registration $entity
     */
    public function testExtraNext(Registration $entity)
    {
        $entity->setNextStatus();

        $this->assertSame(self::STATUS_COMPLETED, $entity->getStatus());
        $this->assertSame(3, $entity->countStatusStep());
    }
}
