<?php

declare(strict_types=1);

namespace App\Service\RegistrationProcessor;

use App\Entity\Registration;
use Twig\Environment as TwigEnvironment;

abstract class AbstractRegistrationProcessor implements RegistrationProcessorInterface
{
    /**
     * @var TwigEnvironment
     */
    protected $templatingEnv;

    /**
     * @var Registration|null
     */
    protected $registration;

    /**
     * @var int
     */
    protected $currentStep;

    /**
     * @param TwigEnvironment $templatingEnv
     */
    public function __construct(TwigEnvironment $templatingEnv)
    {
        $this->templatingEnv = $templatingEnv;
    }

    /**
     * @param Registration|null $registration
     * @param int               $currentStep
     */
    public function setFlow(?Registration $registration, int $currentStep = 0): void
    {
        $this->registration = $registration;
        $this->currentStep = $currentStep;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->templatingEnv->render(
            $this->getView(),
            $this->getViewParameters()
        );
    }

    /**
     * @return int|null
     */
    public function getFlowId(): ?int
    {
        return $this->registration->getId() ?? null;
    }

    /**
     * @return int
     */
    public function getFlowStep(): int
    {
        return $this->registration->countStatusStep() ?? 0;
    }

    /**
     * @return int
     */
    public function getCurrentStep(): int
    {
        return $this->currentStep;
    }

    /**
     * @return array
     */
    protected function getViewParameters(): array
    {
        return [
            'registration' => $this->registration,
        ];
    }

    /**
     * @return string
     */
    abstract protected function getView(): string;
}
