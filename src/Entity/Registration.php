<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RegistrationRepository")
 */
class Registration
{
    const STATUS_ADDRESS = 'ADDRESS';
    const STATUS_PROFILE = 'PROFILE';
    const STATUS_PAYMENT = 'PAYMENT';
    const STATUS_COMPLETED = 'COMPLETED';

    const STATUSES_FLOW = [
        self::STATUS_PROFILE,
        self::STATUS_ADDRESS,
        self::STATUS_PAYMENT,
        self::STATUS_COMPLETED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=255)
     * @Assert\NotBlank(groups={"profile"})
     *
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255)
     * @Assert\NotBlank(groups={"profile"})
     *
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"profile"})
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"address"})
     *
     * @var string
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\NotBlank(groups={"address"})
     *
     * @var string
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\NotBlank(groups={"address"})
     *
     * @var string
     */
    private $city;

    /**
     * @ORM\Column(name="account_owner", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"payment"})
     *
     * @var string
     */
    private $accountOwner;

    /**
     * @ORM\Column(type="string", length=34, nullable=true)
     * @Assert\NotBlank(groups={"payment"})
     * @Assert\Length(
     *     min="5",
     *     max="34",
     *     minMessage="IBAN must be not less than 5.",
     *     maxMessage="IBAN must be not greater than 34."
     * )
     *
     * @var string
     */
    private $iban;

    /**
     * @ORM\Column(name="payment_data_id", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $paymentDataId;

    /**
     * @ORM\Column(type="string", length=32)
     *
     * @var string
     */
    private $status;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = clone $this->createdAt;
        $this->status = \current(self::STATUSES_FLOW);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAccountOwner(): ?string
    {
        return $this->accountOwner;
    }

    public function setAccountOwner(?string $accountOwner): self
    {
        $this->accountOwner = $accountOwner;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(?string $paymentDataId): self
    {
        $this->paymentDataId = $paymentDataId;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setNextStatus(): self
    {
        $nextStep = $this->countStatusStep() + 1;
        $this->status = self::STATUSES_FLOW[
            \min($nextStep, \count(self::STATUSES_FLOW) - 1)
        ];

        return $this;
    }

    public function countStatusStep(): int
    {
        return \array_search($this->status, self::STATUSES_FLOW) ?: 0;
    }
}
